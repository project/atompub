<?php
/**
 * @file
 * Provides AtompubCollection class
 */

class AtompubCollection extends AtompubBase implements GET, POST {

  /**
   * @var string The title of this collection
   */
  private $title = '';

  /**
   * @var string The URL of this collection
   */
  private $url = '';

  /**
   * @var DateTime The date of last update to an entry of this collection
   */
  private $updated = NULL;

  /**
   * @var array The applicable categories for this collection
   */
  private $categories = array();

  /**
   * @var LimitIterator
   */
  private $documents = NULL;

  /**
   * @var int The maximum number of entries returned in one request
   */
  private $entriesPerPage = 10;

  /**
   * @var string The name of a function to call when an entry is received
   */
  private $saveCallback = '';

  public function get() {
    $page = new DomDocument();
    $feed = $page->createElementNS(NS_ATOM, 'feed');
    $page->appendChild($feed);

    $feed->appendChild($page->createElementNS(NS_ATOM, 'title', $this->title));

    $link = $page->createElementNS(NS_ATOM, 'link');
    $link->setAttribute('rel', 'self');
    $link->setAttribute('href', $this->url);
    $feed->appendChild($link);

    $feed->appendChild($page->createElementNS(NS_ATOM, 'id', $this->url));
    $feed->appendChild($page->createElementNS(NS_ATOM, 'updated', $this->updated->format(DATE_ATOM)));

    foreach ($this->documents as $document) {
      $feed->appendChild($page->importNode($document->createEntry(FALSE)->firstChild, TRUE));
    }

    if (count($this->documents->getInnerIterator()) > $this->documents->getPosition()) {
      $link = $page->createElementNS(NS_ATOM, 'link');
      $link->setAttribute('rel', 'next');
      $link->setAttribute('href', $this->nextPage());
      $feed->appendChild($link);
    }

    $headers = array('Content-Type' => 'application/atom+xml;type=feed');

    return array('headers' => $headers, 'body' => $page);
  }

  public function post($type, $content) {
    if (isset($type) && $type != 'application/atom+xml;type=entry' && $type != 'application/atom+xml') {
      throw new AtompubUnsupportedMediaType($type);
    }

    $document = AtompubDocument::createFromXml($content);

    call_user_func($this->saveCallback, $document, $this->url);

    $headers = array(
      'Content-Type' => 'application/atom+xml;type=entry',
      'Status' => '201 Created',
      'Location' => $document->getEditUrl(),
    );

    return array('headers' => $headers, 'body' => $document->createEntry());
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function getUrl() {
    return $this->url;
  }

  public function setUpdated(DateTime $updated) {
    $this->updated = $updated;
  }

  public function addCategory($category) {
    $this->categories[] = $category;
  }

  public function setCategories(array $categories) {
    $this->categories = $categories;
  }

  public function getCategories() {
    return $this->categories;
  }

  public function setDocuments(LimitIterator $documents) {
    $this->documents = $documents;
  }

  public function setEntriesPerPage($entries) {
    $this->entriesPerPage = $entries;
  }

  public function getEntriesPerPage() {
    return $this->entriesPerPage;
  }

  public function setSaveCallback($saveCallback) {
    $this->saveCallback = $saveCallback;
  }

  private function nextPage() {
    $page = floor($this->documents->getPosition() / $this->entriesPerPage);
    return "$this->url/$page";
  }
}
