<?php
/**
 * @file
 * Provides AtompubCategories class
 */

class AtompubCategories extends AtompubBase implements GET {

  private $url = '';

  private $terms = array();

  private $fixed = FALSE;

  public function get() {
    $page = new DomDocument();
    $cats = $page->createElementNS(NS_APP, 'categories');
    $cats->setAttribute('fixed', $this->fixed ? 'yes' : 'no');
    $cats->setAttribute('scheme', $this->getUrl());
    $cats->setAttributeNS(NS_XMLNS, 'xmlns:atom', NS_ATOM);
    $page->appendChild($cats);

    foreach ($this->terms as $term) {
      $cat = $page->createElementNS(NS_ATOM, 'atom:category');
      $cat->setAttribute('term', $term);
      $cats->appendChild($cat);
    }

    $headers = array('Content-Type' => 'application/atomcat+xml');

    return array('headers' => $headers, 'body' => $page);
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function getUrl() {
    return $this->url;
  }

  public function addTerm($term) {
    $this->terms[] = $term;
  }

  public function setTerms(array $terms) {
    $this->terms = $terms;
  }

  public function setFixed($fixed) {
    $this->fixed = $fixed;
  }

}
