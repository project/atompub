<?php

require_once 'atompub_base.inc';
require_once 'atompub_service.inc';
require_once 'atompub_collection.inc';
require_once 'atompub_categories.inc';

class AtompubCategoriesTest extends PHPUnit_Framework_TestCase {

  public function test_get() {
    $expected_xml = <<<EOD
<?xml version="1.0" ?>
<categories
    xmlns="http://www.w3.org/2007/app"
    xmlns:atom="http://www.w3.org/2005/Atom"
    fixed="yes" scheme="http://example.com/atom/categories/big3">
  <atom:category term="animal" />
  <atom:category term="vegetable" />
  <atom:category term="mineral" />
</categories>
EOD;

    $categories = new AtompubCategories();
    $categories->setUrl('http://example.com/atom/categories/big3');
    $categories->addTerm('animal');
    $categories->addTerm('vegetable');
    $categories->addTerm('mineral');
    $categories->setFixed(TRUE);

    $result = $categories->get();
    $this->assertEquals('application/atomcat+xml', $result['headers']['Content-Type']);
    $this->assertXmlStringEqualsXmlString($expected_xml, $result['body']->saveXML());
  }
}
