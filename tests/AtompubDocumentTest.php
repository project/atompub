<?php

require_once 'atompub_base.inc';
require_once 'atompub_service.inc';
require_once 'atompub_collection.inc';
require_once 'atompub_categories.inc';
require_once 'atompub_document.inc';

class AtompubDocumentTest extends PHPUnit_Framework_TestCase {

  private $base_url = 'http://example.org';

  public function test_createFromXml() {
    $title = 'My First Entry';
    $updated = new DateTime();
    $updatedAtom = $updated->format(DATE_ATOM);
    $edit_url = "$this->base_url/atom/collection/blog/my-first-entry";
    $url = "$this->base_url/blog/my-first-entry";
    $author_name = "Stefan Freudenberg";
    $author_email = "stefan@agaric.com";
    $author_uri = "$this->base_url/user/stefan";
    $content = "Hello World\n\nLorem ipsum dolor sit amectetur...";

    $xml = <<<EOD
<?xml version="1.0" ?>
<entry xmlns="http://www.w3.org/2005/Atom">
  <title>$title</title>
  <link href="$url" rel="alternate"/>
  <link href="$edit_url" rel="edit" />
  <id>$edit_url</id>
  <updated>$updatedAtom</updated>
  <author>
    <name>$author_name</name>
    <email>$author_email</email>
    <uri>$author_uri</uri>
  </author>
  <content type="html">$content</content>
</entry>
EOD;

    $document = AtompubDocument::createFromXml($xml);
    $this->assertEquals($title, $document->getTitle());
    $this->assertEquals($content, $document->getContent());
    $this->assertEquals($updated->getTimestamp(), $document->getUpdated()->getTimestamp());
    $this->assertEquals($author_name, $document->getAuthorName());
  }

  public function test_get() {
    $title = 'My First Entry';
    $updated = new DateTime();
    $updatedAtom = $updated->format(DATE_ATOM);
    $edit_url = "$this->base_url/atom/collection/blog/my-first-entry";
    $url = "$this->base_url/blog/my-first-entry";
    $author_name = "Stefan Freudenberg";
    $author_email = "stefan@agaric.com";
    $author_uri = "$this->base_url/user/stefan";
    $content = "Hello World\n\nLorem ipsum dolor sit amectetur...";
    $terms = array('vegetable', 'homegrown');

    $expected_xml = <<<EOD
<?xml version="1.0" ?>
<entry xmlns="http://www.w3.org/2005/Atom">
  <title>$title</title>
  <link href="$url" rel="alternate"/>
  <link href="$edit_url" rel="edit" />
  <id>$edit_url</id>
  <updated>$updatedAtom</updated>
  <author>
    <name>$author_name</name>
    <email>$author_email</email>
    <uri>$author_uri</uri>
  </author>
  <content type="html">$content</content>
  <category term="$terms[0]" />
  <category term="$terms[1]" />
</entry>
EOD;

    $document = new AtompubDocument();
    $document->setTitle($title);
    $document->setUrl($url);
    $document->setEditUrl($edit_url);
    $document->setUpdated($updated);
    $document->setAuthorName($author_name);
    $document->setAuthorEmail($author_email);
    $document->setAuthorUri($author_uri);
    $document->setContent($content);
    $document->setTerms($terms);

    $result = $document->get();
    $this->assertEquals('application/atom+xml;type=entry', $result['headers']['Content-Type']);
    $this->assertXmlStringEqualsXmlString($expected_xml, $result['body']->saveXML());
  }

  public function test_delete() {
    $document = new AtompubDocument();
    $document->setDeleteCallback(function ($document) {});
    $result = $document->delete();
    $this->assertEquals('204 No Content', $result['headers']['Status']);
  }

}
