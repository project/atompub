<?php

require_once 'atompub_base.inc';
require_once 'atompub_service.inc';
require_once 'atompub_collection.inc';
require_once 'atompub_categories.inc';

class AtompubServiceTest extends PHPUnit_Framework_TestCase {

  private $base_url = 'http://example.org';

  public function test_get() {
    $expected_xml = <<<EOD
<?xml version="1.0"?>
<service xmlns="http://www.w3.org/2007/app"
        xmlns:atom="http://www.w3.org/2005/Atom">
  <workspace>
    <atom:title>Main Site</atom:title>
    <collection
        href="http://example.org/atom/collection/blog" >
      <atom:title>My Blog Entries</atom:title>
      <categories
        href="http://example.org/atom/cats/forMain" />
    </collection>
  </workspace>
</service>
EOD;

    $categories = new AtompubCategories();
    $categories->setUrl("$this->base_url/atom/cats/forMain");

    $collection = new AtompubCollection();
    $collection->setTitle('My Blog Entries');
    $collection->setUrl("$this->base_url/atom/collection/blog");
    $collection->setUpdated(new DateTime());
    $collection->addCategory($categories);

    $service = new AtompubService();
    $service->setTitle('Main Site');
    $service->addCollection($collection);

    $result = $service->get();
    $this->assertEquals('application/atomsvc+xml', $result['headers']['Content-Type']);
    $this->assertXmlStringEqualsXmlString($expected_xml, $result['body']->saveXml());
  }
}

