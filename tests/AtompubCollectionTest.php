<?php

require_once 'atompub_base.inc';
require_once 'atompub_service.inc';
require_once 'atompub_collection.inc';
require_once 'atompub_categories.inc';

class AtompubCollectionTest extends PHPUnit_Framework_TestCase {

  private $base_url = 'http://example.org';

  public function test_get() {
    $title = 'Blog Entry';
    $updated = new DateTime();
    $updatedAtom = $updated->format(DATE_ATOM);
    $url = "$this->base_url/atom/collection/blog";

    $entry_title = 'My First Entry';
    $entry_edit_url = "$this->base_url/atom/collection/blog/my-first-entry";
    $entry_url = "$this->base_url/blog/my-first-entry";
    $author_name = "Stefan Freudenberg";
    $author_email = "stefan@agaric.com";
    $author_uri = "$this->base_url/user/stefan";
    $entry_content = "Hello World\n\nLorem ipsum dolor sit amectetur...";
    $entry_terms = array('vegetable', 'homegrown');

    $entry = <<<EOD
<entry>
  <title>$entry_title</title>
  <link href="$entry_url" rel="alternate"/>
  <link href="$entry_edit_url" rel="edit" />
  <id>$entry_edit_url</id>
  <updated>$updatedAtom</updated>
  <author>
    <name>$author_name</name>
    <email>$author_email</email>
    <uri>$author_uri</uri>
  </author>
  <category term="$entry_terms[0]" />
  <category term="$entry_terms[1]" />
</entry>
EOD;

    $expected_xml = <<<EOD
<?xml version="1.0" ?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>$title</title>
  <link rel="self" href="$url" />
  <id>$url</id>
  <updated>$updatedAtom</updated>
  $entry
  $entry
  $entry
  $entry
  $entry
  $entry
  $entry
  $entry
  $entry
  $entry
  <link rel="next" href="$url/1" />
</feed>
EOD;

    $document = new AtompubDocument();
    $document->setTitle($entry_title);
    $document->setUrl($entry_url);
    $document->setEditUrl($entry_edit_url);
    $document->setUpdated($updated);
    $document->setAuthorName($author_name);
    $document->setAuthorEmail($author_email);
    $document->setAuthorUri($author_uri);
    $document->setTerms($entry_terms);
    $document->setContent($entry_content);

    $collection = new AtompubCollection();
    $collection->setTitle($title);
    $collection->setUrl($url);
    $collection->setUpdated($updated);
    $collection->setDocuments(new LimitIterator(new ArrayIterator(array_fill(0, 11, $document)), 0, 10));

    $result = $collection->get();
    $this->assertEquals('application/atom+xml;type=feed', $result['headers']['Content-Type']);
    $this->assertXmlStringEqualsXmlString($expected_xml, $result['body']->saveXML());
  }

  public function test_post() {
    $type = 'application/atom+xml;type=entry';
    $edit_url = "$this->base_url/atom/document/1";

    $title = 'My First Entry';
    $author_name = 'Stefan Freudenberg';
    $updated = new DateTime();
    $updatedAtom = $updated->format(DATE_ATOM);
    $entry = <<< EOD
<entry xmlns="http://www.w3.org/2005/Atom">
  <title>$title</title>
  <updated>$updatedAtom</updated>
  <author>
    <name>$author_name</name>
  </author>
</entry>
EOD;

    $callback = function (AtompubDocument $document) use ($edit_url) {
      $document->setEditUrl($edit_url);
    };

    $collection = new AtompubCollection();
    $collection->setSaveCallback($callback);

    $result = $collection->post($type, $entry);
    $this->assertEquals($type, $result['headers']['Content-Type']);
    $this->assertEquals('201 Created', $result['headers']['Status']);
    $this->assertEquals($edit_url, $result['headers']['Location']);
  }

  /**
   * @expectedException AtompubUnsupportedMediaType
   */
  public function test_post_exception() {
    $collection = new AtompubCollection();
    $collection->post('text/plain', 'test');
  }

}
