<?php

define('NS_XMLNS', 'http://www.w3.org/2000/xmlns/');

define('NS_APP', 'http://www.w3.org/2007/app');

define('NS_ATOM', 'http://www.w3.org/2005/Atom');

interface GET {
  public function get();
}

interface POST {
  public function post($type, $content);
}

interface PUT {
  public function put($type, $content);
}

interface DELETE {
  public function delete();
}

abstract class AtompubException extends Exception {

  protected $reason_phrase;

  protected $headers = array();

  public function __construct($status_code = NULL, $reason_phrase = NULL, $message = NULL) {
    parent::__construct($message, $status_code);
    $this->reason_phrase = $reason_phrase;
    $this->headers['Status'] = $this->getStatusLine();
  }

  public function __toString() {
    return $this->getStatusLine();
  }

  public function getStatusLine() {
    return sprintf('%d %s', $this->code, $this->reason_phrase);
  }

  public function getHeaders() {
    return $this->headers;
  }
}

class AtompubBadRequest extends AtompubException {
  public function __construct($message = NULL) {
    parent::__construct(400, 'Bad Request', $message);
  }
}

class AtompubUnauthorized extends AtompubException {
  public function __construct($realm, $message = NULL) {
    parent::__construct(401, 'Unauthorized', $message);
    $this->headers['WWW-Authenticate'] = "Basic realm: \"$realm\"";
  }
}

class AtompubForbidden extends AtompubException {
  public function __construct($message = NULL) {
    parent::__construct(403, 'Forbidden', $message);
  }
}

class AtompubNotFound extends AtompubException {
  public function __construct($message = NULL) {
    parent::__construct(404, 'Not Found', $message);
  }
}

class AtompubMethodNotAllowed extends AtompubException {
  public function __construct($allowed = array()) {
    parent::__construct(405, 'Method Not Allowed', $message);
    $this->headers['Allowed'] = implode(', ', $allowed);
  }
}

class AtompubUnsupportedMediaType extends AtompubException {
  public function __construct($message = NULL) {
    parent::__construct(415, 'Unsupported Media Type', $message);
  }
}

class AtompubInternalServerError extends AtompubException {
  public function __construct(Exception $e) {
    parent::__construct(500, 'Internal Server Error', $e->getMessage());
  }
}

class AtompubServiceUnavailable extends AtompubException {
  public function __construct(Exception $e) {
    parent::__construct(503, 'ServiceUnavailable', $e->getMessage());
  }
}

abstract class AtompubBase {

  public function __invoke() {
    $method = $_SERVER['REQUEST_METHOD'];

    if (!array_key_exists($method, class_implements($this))) {
      throw new AtompubMethodNotAllowed(class_implements($this));
    }

    if ($method == 'POST' || $method == 'PUT') {
      return $this->$method($_SERVER['CONTENT_TYPE'], file_get_contents('php://input'));
    }
    else {
      return $this->$method();
    }
  }

}
