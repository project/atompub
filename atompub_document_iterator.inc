<?php
/**
 * @file
 * Provides an ArrayIterator for documents
 */

class AtompubDocumentIterator extends ArrayIterator {
  public function current() {
    $node = parent::current();
    return atompub_document_create($node);
  }
}
