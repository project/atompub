<?php
/**
 * @file
 * Provides AtompubService class
 */

class AtompubService extends AtompubBase implements GET {

  private $title = '';

  private $collections = array();

  public function get() {
    $page = new DomDocument();
    $service = $page->createElementNS(NS_APP, 'service');
    $service->setAttributeNS(NS_XMLNS, 'xmlns:atom', NS_ATOM);
    $page->appendChild($service);
    $workspace = $page->createElementNS(NS_APP, 'workspace');
    $service->appendChild($workspace);
    $workspace->appendChild($page->createElementNS(NS_ATOM, 'atom:title', $this->title));

    foreach ($this->collections as $collection) {
      $element = $page->createElementNS(NS_APP, 'collection');
      $workspace->appendChild($element);
      $element->appendChild($page->createElementNS(NS_ATOM, 'atom:title', $collection->getTitle()));
      $element->setAttribute('href', $collection->getUrl());

      foreach ($collection->getCategories() as $categories) {
        $categories_element = $page->createElementNS(NS_APP, 'categories');
        $categories_element->setAttribute('href', $categories->getUrl());
        $element->appendChild($categories_element);
      }
    }

    $headers = array('Content-Type' => 'application/atomsvc+xml');

    return array('headers' => $headers, 'body' => $page);
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function addCollection(AtompubCollection $collection) {
    $this->collections[] = $collection;
  }

  public function setCollections(array $collections) {
    $this->collections = $collections;
  }

}
