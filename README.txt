-- SUMMARY --

This module exposes Drupal content via the Atom Publishing Protocol, allowing 
data retrieval and manipulation by clients supporting the protocol.

For a full description of the module, visit the project page:
  http://drupal.org/project/atompub

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/atompub


-- REQUIREMENTS --

* PHP >= 5.3


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONTACT --

Current maintainers:
* Stefan Freudenberg - http://drupal.org/user/386087
