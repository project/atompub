<?php
/**
 * @file
 * Provides AtompubDocument class
 */

class AtompubDocument extends AtompubBase implements GET, PUT, DELETE {

  private $title = '';

  private $url = '';

  private $editUrl = '';

  private $updated = NULL;

  private $author = array('name' => '', 'email' => '', 'uri' => '');

  private $content = '';

  private $terms = array();

  private $deleteCallback = NULL;

  private $updateCallback = NULL;

  /**
   * Creates an instance of AtompubDocument from the given entry XML.
   *
   * @param DomDocument $entry
   *
   * @return AtompubDocument
   */
  public static function createFromXml($xml) {
    $entry = new DomDocument();
    $entry->loadXml($xml);
    $xpath = new DomXpath($entry);
    $xpath->registerNamespace('atom', NS_ATOM);

    $document = new AtompubDocument();
    $document->setTitle($xpath->evaluate('string(//atom:title)'));
    $document->setContent($xpath->evaluate('string(//atom:content)'));
    $document->setAuthorName($xpath->evaluate('string(//atom:author/atom:name)'));

    $result = $xpath->query('//atom:updated');
    if ($result->length == 1) {
      $document->setUpdated(DateTime::createFromFormat(DATE_ATOM, $result->item(0)->textContent));
    }

    return $document;
  }

  public function get() {
    $headers = array('Content-Type' => 'application/atom+xml;type=entry');

    return array('headers' => $headers, 'body' => $this->createEntry());
  }

  public function put($type, $content) {
    if (isset($type) && $type != 'application/atom+xml;type=entry' && $type != 'application/atom+xml') {
      throw new AtompubUnsupportedMediaType($type);
    }

    call_user_func($this->updateCallback, $this);

    return array('headers' => array(), 'body' => $this->createEntry());
  }

  public function delete() {
    call_user_func($this->deleteCallback, $this);

    $headers = array('Status' => '204 No Content');

    return array('headers' => $headers, 'body' => NULL);
  }

  public function createEntry($with_content = TRUE) {
    $document = new DomDocument();
    $entry = $document->createElementNS(NS_ATOM, 'entry');
    $document->appendChild($entry);

    $entry->appendChild($document->createElementNS(NS_ATOM, 'title', $this->getTitle()));

    $link = $document->createElementNS(NS_ATOM, 'link');
    $link->setAttribute('href', $this->getUrl());
    $link->setAttribute('rel', 'alternate');
    $entry->appendChild($link);

    $edit_link = $document->createElementNS(NS_ATOM, 'link');
    $edit_link->setAttribute('href', $this->getEditUrl());
    $edit_link->setAttribute('rel', 'edit');
    $entry->appendChild($edit_link);

    $entry->appendChild($document->createElementNS(NS_ATOM, 'id', $this->getEditUrl()));

    $entry->appendChild($document->createElementNS(NS_ATOM, 'updated', $this->getUpdated()->format(DATE_ATOM)));

    $author = $document->createElementNS(NS_ATOM, 'author');
    $entry->appendChild($author);
    $author->appendChild($document->createElementNS(NS_ATOM, 'name', $this->getAuthorName()));
    $author->appendChild($document->createElementNS(NS_ATOM, 'email', $this->getAuthorEmail()));
    $author->appendChild($document->createElementNS(NS_ATOM, 'uri', $this->getAuthorUri()));

    if ($with_content) {
      $content = $document->createElementNS(NS_ATOM, 'content');
      $content->setAttribute('type', 'html');
      $entry->appendChild($content);
      $content->appendChild($document->createTextNode($this->getContent()));
    }

    foreach ($this->terms as $term) {
      $element = $document->createElementNS(NS_ATOM, 'category');
      $element->setAttribute('term', $term);
      $entry->appendChild($element);
    }

    return $document;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function getUrl() {
    return $this->url;
  }

  public function setEditUrl($editUrl) {
    $this->editUrl = $editUrl;
  }

  public function getEditUrl() {
    return $this->editUrl;
  }

  public function setUpdated(DateTime $updated) {
    $this->updated = $updated;
  }

  public function getUpdated() {
    return $this->updated;
  }

  public function setAuthor(array $author) {
    $this->author = $author;
  }

  public function setAuthorName($name) {
    $this->author['name'] = $name;
  }

  public function getAuthorName() {
    return $this->author['name'];
  }

  public function setAuthorEmail($email) {
    $this->author['email'] = $email;
  }

  public function getAuthorEmail() {
    return $this->author['email'];
  }

  public function setAuthorUri($uri) {
    $this->author['uri'] = $uri;
  }

  public function getAuthorUri() {
    return $this->author['uri'];
  }

  public function setContent($content) {
    $this->content = $content;
  }

  public function getContent() {
    return $this->content;
  }

  public function addTerm($term) {
    $this->terms[] = $term;
  }

  public function setTerms(array $terms) {
    $this->terms = $terms;
  }

  public function setDeleteCallback($deleteCallback) {
    $this->deleteCallback = $deleteCallback;
  }

  public function setUpdateCallback($updateCallback) {
    $this->updateCallback = $updateCallback;
  }

}
